/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task.manager;

import http.client.SecurityClient;

import java.io.File;
import java.util.concurrent.Callable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author u0163889
 */
public class EncryptionTask implements Callable {
    
    private final File file;
    private final SecurityClient securityClient;
    private final Logger logger;
        
    public EncryptionTask(File file) {
        this.file = file;
        logger = LogManager.getLogger(EncryptionTask.class);
        securityClient = new SecurityClient();
    }
    
    @Override
    public StringBuilder call() throws Exception {
        logger.info("\tAbout to encrypt file - " + file.getName());
        String newFileName = securityClient.doPostFile(SecurityClient.ENCRYPT, file);
        if (newFileName == null) {
            logger.error("\tError in EncryptionTask. Return value was null");
            return null;
        }
        logger.info("\tTask complete result is - " + newFileName);
        return new StringBuilder(newFileName);
    }

}
