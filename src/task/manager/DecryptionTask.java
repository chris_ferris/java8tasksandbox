/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task.manager;

import http.client.SecurityClient;
import java.io.File;
import java.util.concurrent.Callable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author u0163889
 */
public class DecryptionTask implements Callable {
    
    private final File file;
    private final SecurityClient securityClient;
    private final Logger logger;
    
    public DecryptionTask(File file) {
        this.file = file;
        logger = LogManager.getLogger(EncryptionTask.class);
        securityClient = new SecurityClient();
    }
    
    @Override
    public StringBuilder call() throws Exception {
        logger.info("\tAbout to decrypt file - " + file.getName());
        String result = securityClient.doPostFile(SecurityClient.DECRYPT, file);
        if (result == null) {
            logger.error("\tError in DecryptionTask. Return value was null");
            return null;
        }
        logger.info("\tTask complete result is - " + result);
        return new StringBuilder(result);
    }
}
