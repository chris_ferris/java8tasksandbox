/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task.manager;

import http.client.SecurityClient;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author u0163889
 */
public class SchedualTasks {
    
    private final ScheduledExecutorService executor;
    private ScheduledFuture<?> future;
    private Map<String, ScheduledFuture<?>> futures;  
    
    private static List<String> decryptionList;
    private int initialDelay = 0;
    
    private final Logger logger;
    
    // The constructor
    public SchedualTasks() {
        logger = LogManager.getLogger(SchedualTasks.class);
        logger.info("Constructing the SchedualTasks object now ");
        executor = Executors.newScheduledThreadPool(4);
        futures = new HashMap<>();
        decryptionList = new ArrayList();
    }
    
    public void submitEncryptionTasks(String path) {
        //  Get list of files to encrypt
        initialDelay = 0;
        futures.clear();
        List<File> fileList = getFileList(path, "", SecurityClient.ENCRYPTED_SUFIX);
        // Check if any files where found. 
        if (fileList.isEmpty()) {
            // Validate the path
            if (Files.notExists(Paths.get(path))) {
                logger.error("The given dir does not exist - " + path);
            } else {
                logger.error("No files where found in the given dir - " + path);
            }
        }
        fileList.forEach(file -> {
            future = executor.schedule(new EncryptionTask(file), initialDelay++, TimeUnit.SECONDS);
            futures.put(file.getName(), future);
        });
        //  Loop until all tasks are finished
        List<String> doneList = new ArrayList();
        while (!futures.isEmpty()) {
            doneList.clear(); 
            Iterator<String> names = futures.keySet().iterator();
            names.forEachRemaining(name -> {
                future = futures.get(name);
                if (future.isDone()) {
                    //  We can remove this.  But do it outside the iterator
                    //  otherwise it causes a ConcurrentModificationException
                    doneList.add(name);
                    try {
                        StringBuilder result = (StringBuilder)future.get();
                        if (result != null) {
                            logger.info("\t..task completed for " + name +
                                        " result is " + result.toString());
                            // Save file name for decyption task. 
                            decryptionList.add(result.toString());
                        }
                    } catch(InterruptedException | 
                            ExecutionException nuts) {
                        logger.error("Got exception in submitEncryptionTasks: " + 
                                     nuts.getMessage());
                    }
                } else {
                    // TODO maybe pause a bit
                    try {
                        Thread.sleep(1000); // Wait 1 seconds
                    } catch(InterruptedException ignore) {}
                } 
            });
            // Remove the instances that have completed. 
            doneList.forEach(name -> futures.remove(name));
        }
    }
    
    public void submitDecryptionTasks() {
        //  Get list of files to encrypt
        initialDelay = 2;
        if (decryptionList.isEmpty()) {
            logger.error("The list if files to decrypt is empty");
            return;
        }
        decryptionList.forEach(pathToFile -> {
            File file = new File(pathToFile);
            if (file.exists() && file.isFile()) {
                future = executor.schedule(new DecryptionTask(file), initialDelay, TimeUnit.SECONDS);
                futures.put(pathToFile, future);
            } else {
                logger.error("File not found to decrypt: " + pathToFile);
            }
        });
        //  Loop until all tasks are finished
        List<String> doneList = new ArrayList();
        while (!futures.isEmpty()) {
            doneList.clear();
            Iterator<String> names = futures.keySet().iterator();
            names.forEachRemaining(name -> {
                future = futures.get(name);
                if (future.isDone()) {
                    //  We can remove this.  But do it outside the iterator
                    //  otherwise it causes a ConcurrentModificationException
                    doneList.add(name);
                     //  Also remove from decryption list.
                    decryptionList.remove(name);
                    try {
                        StringBuilder result = (StringBuilder)future.get();
                        if (result != null) {
                            logger.info("\t..task completed for " + name +
                                        " result is " + result.toString());
                        }
                    } catch(InterruptedException | 
                             ExecutionException nuts) {
                         logger.error("Got exception in submitDecryptionTasks: " + 
                                      nuts.getMessage());
                    }
                } else {
                    // TODO maybe pause a bit
                    try {
                        Thread.sleep(1500);  // Wait 1.5 seconds
                    } catch(InterruptedException ignore) {}
                } 
            });
            // Remove the instances that have completed. 
            doneList.forEach(name -> futures.remove(name));            
        }
    }

    public void shutDown() {
        executor.shutdown();
    }
    
    public boolean isDecryptionListEmpty() {
        return decryptionList.isEmpty();
    }
    
    public List<String> getDecryptionList() {
        return decryptionList;
    }

    public void setDecryptionList(List<String> theList) {
        decryptionList = theList;
    }
    
    protected List<File> getFileList(String lookIn, String sufix, String notSufix) {
       
        List<File> fileList = new ArrayList<>();
        
        // Validate the path
        Path path = Paths.get(lookIn);
        File[] list = path.toFile().listFiles();
        // Make sure we got at least one file. 
        if ((list != null) && (list.length > 0)) {
            Stream<File> fileStream = Arrays.stream(list);
            fileStream.forEach(file -> {
                if (file.isFile()) {
                    String lowerCaseName = file.getName().toLowerCase();
                    if ((sufix.isEmpty() || lowerCaseName.endsWith(sufix)) &&
                        (notSufix.isEmpty() || !lowerCaseName.endsWith(notSufix))) {
                        fileList.add(file);
                    }
                }
            });
        }
        return fileList;
    }
}
