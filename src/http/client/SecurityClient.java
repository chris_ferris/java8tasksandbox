/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package http.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.apache.http.Consts;

import static org.apache.http.HttpHeaders.USER_AGENT;
import static org.apache.http.HttpStatus.SC_OK;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.core.util.IOUtils;

/**
 *
 * @author u0163889
 */
public class SecurityClient {
    
    private CloseableHttpClient closeableClient = null;
    private final Map<String, URI> uriMap;
    private final Map<String, String> uriParameters;
    
    private final Logger logger;
    private final Marker ohNuts;
    
    public static final String TEST = "test";
    public static final String ENCRYPT = "encrypt";
    public static final String DECRYPT = "decrypt";
    public final static String ENCRYPTED_SUFIX = "encryted";
    
    public static final String SCHEME = "http";
    public static final String HOST = "c908hqwcccbe:9012";
    public static final String[] REQUEST_KEY =
        {TEST, ENCRYPT, DECRYPT};
    
    public static final String[] PATH = 
        {"/Security/v1/resourcecheck", 
         "/Security/v1/encrypt/formanswer_key",
         "/Security/v1/decrypt/formanswer_key"};
    
    // The consturctor(s)
    public SecurityClient() {
    
        logger = LogManager.getLogger(SecurityClient.class);
        ohNuts = MarkerManager.getMarker("THIS IS NOT GOOD");
        //  Build uri's and put in map
        closeableClient = HttpClientBuilder.create().build();
        uriMap = new HashMap<>();
        uriParameters = new HashMap<>();
    
        for (int i = 0; i < REQUEST_KEY.length; i++) {
            uriMap.put(REQUEST_KEY[i], buildURI(i, uriParameters));
        }
    }

    public String doPostFile(String keyId, String fileName) throws IOException {
        
        File testFile = new File(fileName);
        return doPostFile(keyId, testFile);
    }    
    
    public String doPostFile(String keyId, File file) throws IOException {
        // First make sure the file exist or is valid. 
        if (!file.exists()) {
            return null;
        }
        // Entity info here
        // https://hc.apache.org/httpcomponents-core-ga/tutorial/html/fundamentals.html
        HttpEntity entity = new FileEntity(file, ContentType.create("application/java-archive"));
        HttpEntity response = doPost(keyId, entity);
        if (keyId.equals(ENCRYPT)) {
            // Change the file name and save the results.
            String encryptedFile = String.join(".", file.getPath(),ENCRYPTED_SUFIX);
            InputStream is = response.getContent();
            if (saveResults(is, encryptedFile)) {
                return encryptedFile;
            }
        } else if (response != null) {
            return String.format("ContentType %s", response.getContentType().getName());
        }
        return null;
    }
    
    public String doPostString(String keyId, String message) throws UnsupportedEncodingException {
        // Entity info here
        // https://hc.apache.org/httpcomponents-core-ga/tutorial/html/fundamentals.html
        HttpEntity entity = new StringEntity(message, ContentType.create("text/plain", Consts.UTF_8));
        HttpEntity response = doPost(keyId, entity);
        try {
            Reader is = new InputStreamReader(response.getContent());
            return IOUtils.toString(is);
        } catch(IOException bummer) {
            logger.error(ohNuts, "IOException in doPostString " +
                         bummer.getMessage());
        } catch(Exception what) {
            logger.error(ohNuts, "Exception in doPostString " +
                         what.getMessage());
        }
        return null;
    }
    
    private HttpEntity doPost(String keyId, HttpEntity entity) {
        
        if (!uriMap.containsKey(keyId)) {
            logger.error("doPost - URI not found for " + keyId);
            return null;
        }
        HttpPost request = new HttpPost(uriMap.get(keyId));
        request.setHeaders(getSetHeaders());
        request.setEntity(entity);
        try {
            CloseableHttpResponse response = closeableClient.execute(request);
            // check status
            int status = response.getStatusLine().getStatusCode();
            if (status != SC_OK) {
                logger.error(ohNuts, "Unwanted return status of " +
                             status + " for URI " +
                             uriMap.get(keyId).toASCIIString()); 
                return null;
            }
            return response.getEntity();
        } catch (IOException ouch) {
            logger.error(ohNuts, "Exception in doPost " +
                         ouch.getMessage() + " for URI " +
                         uriMap.get(keyId).toASCIIString());
        } catch(Exception what) {
            logger.error(ohNuts, "Exception in doPostString " +
                         what.getMessage());
        }
        return null;
    }
    
    public String doGet(String keyId) {
        
        if (!uriMap.containsKey(keyId)) {
            logger.error("doGet - URI not found for " + keyId);
            return "";
        }

        HttpGet request = new HttpGet(uriMap.get(keyId));
        request.addHeader("User-Agent", USER_AGENT);
        try {
            CloseableHttpResponse response = closeableClient.execute(request);
            try {
                // check status
                int status = response.getStatusLine().getStatusCode();
                if (status != SC_OK) {
                    logger.error(ohNuts, "Unwanted return status of " +
                                 status + " for URI " +
                                 uriMap.get(keyId).toASCIIString()); 
                    return null;
                }
                HttpEntity entity = response.getEntity();
                if (entity == null) {
                    return "Error: HttpEntity is null";
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    result.append(line);
                }
                br.close();
                return result.toString();
            } finally {
                response.close();
            }
        } catch(IOException ouch) {
            logger.error(ohNuts, ouch.getMessage());
            logger.error(ohNuts, " for URI " + uriMap.get(keyId).toASCIIString());      
        }
        // Not good if we got this far.        
        return "";
    }
    
    public Map<String, URI> getUriMap() {
        return uriMap;
    }

    private boolean saveResults(InputStream inputStream, String fileName) {
    
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            OutputStream outputStream = new FileOutputStream(file);
            byte[] bytes = new byte[2048];
            int read = inputStream.read(bytes);
            while (read >= 0) {
                outputStream.write(bytes, 0, read);
                read = inputStream.read(bytes);
            }
            outputStream.close();
            return true;
        } catch(IOException ouch) {
            logger.error(ohNuts, ouch.getMessage());
        }
        return false;
    }
    
    /** Build the URI 
     * @return  **/
    private URI buildURI(int index, Map<String, String> params) {
        
        //  Build location/coordinates list first
        List<NameValuePair> locationParameters = new ArrayList<>();
        params.forEach((key, value)->{
            locationParameters.add(new BasicNameValuePair(key, value));
        });        
        URI uri = null;
        try {
            uri = new URIBuilder()
                    .setScheme(SCHEME)
                    .setHost(HOST)
                    .setPath(PATH[index])
                    .addParameters(locationParameters)
                    .build();
        } catch(URISyntaxException nuts) {
            logger.error(ohNuts, nuts.getMessage());
        }
        return uri;
    }
    
    private Header[] getSetHeaders() {
        
        Header[] headers = {
            new BasicHeader(HttpHeaders.ACCEPT,"application/json;"),
            new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8"),
            new BasicHeader("x-cobalt-security-uds: http", "//uds.int.forms.demo.westlaw.com/"),
            new BasicHeader("X-Cobalt-Security-OnePassUserNameEncoded", "Forms34User_Demo"),
            new BasicHeader("Authorization", "username=4C689670C71DD3F7DED5D5F1920F83824FA7559BCF0549DA4AE2FA4B5719FCEF,password=33D0F6B85E826CA746AC08DDDF1B1DB9DAECB740A2EFD4BFACCA764373299335"),
            new BasicHeader("x-cobalt-security-encoding", "base64")
        };
        return headers;
    }
}
