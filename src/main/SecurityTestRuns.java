/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import task.manager.SchedualTasks;

/**
 *
 * @author u0163889
 */
public class SecurityTestRuns {

    private static SchedualTasks runTest;
    private static String dir;
    
    private final static String DEFAULT_DIR = "testFiles";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Check for the dir/path parameter
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("-help") || 
                args[0].equalsIgnoreCase("-h")) {
                showHelp();
                return;
            }
            if ((args.length > 1) && 
                (args[0].equalsIgnoreCase("-dir") || 
                 args[0].equalsIgnoreCase("-d"))) {
                dir = args[1];
            } else {
                dir = DEFAULT_DIR;
            }
        } else {
             dir = DEFAULT_DIR;
        }
        
        System.out.println("starting up SchedualTasks now.");
        System.out.println("processing files in " + dir);
        runTest = new SchedualTasks();
        runTest.submitEncryptionTasks(dir);
        if (runTest.isDecryptionListEmpty()) {
            System.out.println("No files found for decryption testing.");
        } else {
            runTest.submitDecryptionTasks();
            System.out.println("Check log for test results.");
        }
        runTest.shutDown();
        System.out.println("Exiting SecurityTestRuns now.");
    }
    
    private static void showHelp() {
        System.out.println("\t-h -help\tThis help info");
        System.out.println("\t-d -dir \tdirecory to find the test files");
        System.out.println("\t        \tDefaul is " + DEFAULT_DIR);
    }
    
}
