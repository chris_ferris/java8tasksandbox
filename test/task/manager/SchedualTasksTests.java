/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task.manager;

import http.client.SecurityClient;
import java.io.File;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author u0163889
 */
public class SchedualTasksTests {
    
    public static final String DEFAULT_DIR = "test/";
    private final SchedualTasks taskManager = new SchedualTasks();
    
    public SchedualTasksTests() {
    }
    
    @Test
    public void testGetFileList() {
        // C:\Users\U0163889\workspace\NetBeansProjects\SecurityModuleTests
        List<File> fileList = taskManager.getFileList(DEFAULT_DIR, "", SecurityClient.ENCRYPTED_SUFIX);
        assertNotNull("List of files should not be null", fileList);
        assertTrue("List of files should be greater than zero", fileList.size() > 0);
        
        fileList = taskManager.getFileList(DEFAULT_DIR, "txt", "");
        assertNotNull("List of files should not be null", fileList);       
        assertEquals("List of files should be 2", fileList.size(), 2);         
        
        fileList = taskManager.getFileList(DEFAULT_DIR, "foo", "baa");
        assertNotNull("List of files should not be null", fileList);       
        assertTrue("List of files should be empty", fileList.isEmpty());        
    }
    
   @Test
    public void testEncryptionTask() {
        
        try {
            taskManager.submitEncryptionTasks(DEFAULT_DIR);
            List<String> decryptionList = taskManager.getDecryptionList();
            assertFalse("Decryption list should not be empty", decryptionList.isEmpty());
            // Now decrypt the file we just encrypted. 
            taskManager.submitDecryptionTasks();
            decryptionList = taskManager.getDecryptionList();
            assertTrue("Decryption list should now be empty", decryptionList.isEmpty());
        } catch(Exception failed) {
            System.out.println("failed: " + failed.getMessage());
            System.out.println(failed.toString());
            fail("failed on exception!");
        }
    }
}
