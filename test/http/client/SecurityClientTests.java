/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package http.client;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.net.URI;
import java.util.Map;
import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.*;

/**
 *
 * @author u0163889
 */
public class SecurityClientTests {
    
    private final SecurityClient testClient;
    
    private static final String SIMPLE_TEST = "{'data':['hello Mr Encryption it is me, this is working so far']}";
    private static final String ASCII_FILE = "test/asciiTest.txt";
    
    public SecurityClientTests() {
        testClient = new SecurityClient();
    }
    
    @Test
    // @Ignore
    public void testUriMap() {
        
        Map<String, URI> uriMap = testClient.getUriMap();
        assertNotNull("URI Map should not be null", uriMap);
        assertTrue("URI Map should not be empty", !uriMap.isEmpty());
        
        uriMap.forEach((String keyId, URI uri)->{
            assertNotNull("The URI in the Map should not be null", uri);
            System.out.printf("location: %s uri - %s \n\n", keyId, uri.toASCIIString());
        });
    }
    
    @Test
    // @Ignore
    public void testDoGet() {
        String result = testClient.doGet(SecurityClient.TEST);
        assertNotNull("Result from doGet() should not be null", result);
        assertTrue("Reseult should not be an empty string", result.length() > 0);
        System.out.printf("Result is as follows\n %s \n", result);
    }
    
    @Test
    @Ignore
    public void testDoPostString() {
        try {
            String result = testClient.doPostString(SecurityClient.ENCRYPT, SIMPLE_TEST);
            assertNotNull("Result from doPostString() encrypt should not be null", result);
            assertTrue("Reseult should not be an empty string", result.length() > 0);
            System.out.printf("Result is as follows\n %s \n", result);
            
            // Now lets decrypt it. 
            result = testClient.doPostString(SecurityClient.DECRYPT, result);
            assertNotNull("Result from doPostString() decrypt should not be null", result);
            assertTrue("Reseult should not be an empty string", result.length() > 0);
            System.out.printf("Result is as follows\n %s \n", result);
        } catch(UnsupportedEncodingException fail) {
            fail("Nuts, it failed");
        }
    }  
    
    @Test
    @Ignore
    public void testDoPostFile() {
        try {
            String result = testClient.doPostFile(SecurityClient.ENCRYPT, "test/testFile1");
            assertNotNull("Result from doPostString() encrypt should not be null", result);

        } catch(IOException fail) {
            fail("Nuts, it failed");
        }        
    }
    
    @Test
    @Ignore
    public void forceLoggingValidateDecryptedDataLength() {
        try {
            String result = testClient.doPostString(SecurityClient.ENCRYPT, SIMPLE_TEST);
            assertNotNull("Result from doPostString() encrypt should not be null", result);
            assertTrue("Reseult should not be an empty string", result.length() > 0);
            System.out.printf("Result is as follows\n %s \n", result);
            
            // Now lets remove the a few characters to modifye the length. 
            int endOf = result.indexOf("]}") - 6;
            int startOf = endOf - 3;
            assertTrue("The starting point should not be less then 10", startOf > 9);
            String removeThis = result.substring(startOf, endOf);
            result = result.replace(removeThis, "");
            result = testClient.doPostString(SecurityClient.DECRYPT, result);
            assertNull("Result from doPostString() decrypt should be null", result);
        } catch(UnsupportedEncodingException fail) {
            fail("Nuts, it failed");
        }
    }

    @Test
    // @Ignore
    public void createAciiFile() {
        buildAsciiFileForTesting();
        System.out.printf("Built the ascii file");
    }
    
    protected void buildAsciiFileForTesting() {
        
        try {
            DataOutputStream os = new DataOutputStream(new FileOutputStream(ASCII_FILE));
            os.writeBytes("{'data':['");
            for (byte i = 0; i < 127; i++ ) {
                os.writeByte(i);
            }
            os.writeBytes("']}");
            os.close();
        } catch(IOException ugh) {
            fail("Nuts, creating the ascii test file failed");
        }
    }
}
